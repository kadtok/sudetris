﻿// TODO: DEBUG CODE -- REMOVE (COMMENT OUT) FROM THE FINAL BUILD.
#define ENABLE_DEBUG_BLOCK_CODE

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    // TODO: These values should be flipped and the whole code updated, because the board matrix should be 12x6, i.e. 12 rows by 6 columns.
    public const int BOARD_COL = 6;
    public const int BOARD_ROW = 12;

    [Header("Set Block Prefab")]
    [SerializeField]
    private Block m_BlockPrefab;

    [Header("Board References")]
    [SerializeField]
    private Rigidbody m_Board;
    [SerializeField]
    private float m_BoardMargin;
    //detalhe


    [Header("Object Settings")]
    [SerializeField]
    private int m_MaxNumBlocks = BOARD_COL * BOARD_ROW; // maximun number of blocks // ORLY? :)

    [SerializeField]
    private float m_DropDelay = 1.0f;
    private const float DROP_DELAY_DEC = 0.01f / 1000.0f;

    private BoardSlot[,] m_BoardArray;
    private List<Block> m_BlockPool = new List<Block>();
    private Block m_CurrentBlock;
    [SerializeField]
    private float[] m_BlockCount = new float[10];

    [SerializeField]
    private Text m_ScoreView;
    private int m_GameScore;
    [SerializeField]
    private GameObject m_GameOver;

#if ENABLE_DEBUG_BLOCK_CODE
    [SerializeField]
    private int m_ForceBlockValue = 0;
#endif

    #region GET SET METHODS
    public List<Block> BlockPool
    {
        get { return m_BlockPool; }
    }

    public float DropDelay
    {
        get { return m_DropDelay; }
    }

    public BoardSlot[,] BoardArray
    {
        get { return m_BoardArray; }
    }

    public float[] BlockCount
    {
        get
        {
            return m_BlockCount;
        }

        set
        {
            m_BlockCount = value;
        }
    }

    public int ForceBlockValue
    {
        get
        {
            return m_ForceBlockValue;
        }

        set
        {
            m_ForceBlockValue = value;
        }
    }

    #endregion

    /// <summary>
    /// When a block is placed in the board grid it calls this method, notifying the game
    /// that a new block should be called.
    /// </summary>
    /// <param name="caller"></param>
    public void ClearCurrentBlock(Block caller)
    {
        Debug.Log((caller == null ? "null" : caller.name) + " called ClearCurrentBlock()");

        if (m_CurrentBlock == caller)
        {
            Debug.Log("Clearing current block...");
            m_CurrentBlock = null;
            CallABlock();
        }
    }

    /// <summary>
    /// Calls a new block.
    /// </summary>
    private void CallABlock()
    {
        Debug.Log("CallABlock");

        if (m_CurrentBlock == null && BoardArray[2, 0].BlockValue == 0)
        {
            int index = GetNextAvailableBlock();
            if (index > -1)
            {
                m_BlockPool[index].transform.position = m_BoardArray[2, 0].BoardPosition;
                m_BlockPool[index].gameObject.SetActive(true);
                m_BlockPool[index].Setup(
#if ENABLE_DEBUG_BLOCK_CODE
                    ForceBlockValue
#endif
                    );
                m_CurrentBlock = m_BlockPool[index];
                Debug.Log("m_CurrentBlock: " + m_CurrentBlock);
                ForceBlockValue = 0;
            }
            else
            {
                Debug.LogWarning("Couldn't call a new block -- there is no block prefab instance available in the block pool (which means the whole board is full.");
            }
        }
        else
        {
            Debug.LogWarning("Couldn't call a new block -- it's probably game over.");
            m_GameOver.SetActive(true);
        }
    }

    //<summary> 
    //Resets every game stats, and start a new game. 
    //<summary>
    public void NewGame()
    {
        Debug.Log("Starting a new game");
        m_GameOver.SetActive(false);
        m_GameScore = 0;
        for (int i = 0; i < BlockPool.Count; i++)
        {
            BlockPool[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < BOARD_COL; i++)
        {
            for (int j = 0; j < BOARD_ROW; j++)
            {
                m_BoardArray[i, j].Reset();
            }
        }
        m_DropDelay = 1.0f;
        ClearCurrentBlock(m_CurrentBlock);
    }

    private int GetNextAvailableBlock()
    {
        for (int i = 0; i < m_BlockPool.Count; ++i)
        {
            // Found a block that is available for reuse, return it.
            if (!m_BlockPool[i].gameObject.activeSelf)
                return i;
        }

        // No block available.
        return -1;
    }

    private void StartDropAllBlocks()
    {
        for (int i = 0; i < m_BlockPool.Count; ++i)
        {
            if (m_BlockPool[i].gameObject.activeSelf)
                m_BlockPool[i].StartDropBlock();
        }
    }

    /// <summary>
    /// Instantiates the blocks.
    /// </summary>
    private void InstantiateBlocks()
    {
        for (int i = 0; i < m_MaxNumBlocks; i++)
        {
            Block newBlock = Instantiate(m_BlockPrefab);
            newBlock.name = "BlockPool[" + i + "]";
            newBlock.SetParent(this);
            newBlock.gameObject.SetActive(false);

            m_BlockPool.Add(newBlock);
        }
    }

    /// <summary>
    /// Verifies if there's some "full board" to clean.
    /// </summary>
    private void CheckBoard()
    {
        for (int i = 0; i < BOARD_COL - 2; i++)
        {
            for (int j = 0; j < BOARD_ROW - 2; j++)
            {
                int total = 0;

                // If block at (i,j) is not empty (has zero value), check 3x3 grid.
                if (GetBlockCount(0, i, j) == 0)
                {
                    for (int n = 1; n < 10; n++)
                    {
                        if (GetBlockCount(n, i, j) == 1)
                        {
                            total++;
                        }
                    }
                }

                // Found a valid 3x3 grid (each block has a unique [1..9] value),
                // so mark all of them as "to be removed from board" (DestroyBlock)
                // and increases player's score.
                if (total == 9)
                {
                    for (int k = 0; k < 3; k++)
                    {
                        for (int l = 0; l < 3; l++)
                        {
                            m_BoardArray[i + k, j + l].DestroyBlock = true;
                        }
                    }
                    m_GameScore += 100;

                    ClearCurrentBlock(m_CurrentBlock);
                    StartDropAllBlocks();
                }
            }
        }
    }

    /// <summary>
    /// Returns how many blocks of value <paramref name="blockValue"/> there are in a 3x3 grid.
    /// </summary>
    /// <param name="blockValue"></param>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <returns></returns>
    private int GetBlockCount(int blockValue, int i, int j)
    {
        int total = 0;
        for (int k = 0; k < 3; k++)
        {
            for (int l = 0; l < 3; l++)
            {
                if (m_BoardArray[i + k, j + l].BlockValue == blockValue)
                    total++;
            }
        }
        return total;
    }

    void AddScore()
    {
        if (!m_GameOver.activeInHierarchy)
            m_GameScore++;
        Invoke("AddScore", 0.5f);
    }
    void Start()
    {
        Random.InitState((int)System.DateTime.Now.Ticks);

        m_GameScore = 0;
        AddScore();
        m_GameOver.SetActive(false);
        m_BoardArray = new BoardSlot[BOARD_COL, BOARD_ROW];

        //fills Board array
        for (int i = 0; i < BOARD_COL; i++)
        {
            for (int j = 0; j < BOARD_ROW; j++)
            {
                m_BoardArray[i, j] = new BoardSlot();
            }
        }

        //fills position array
        m_BoardArray[0, 0].BoardPosition = new Vector3(m_Board.position.x + m_BoardMargin, m_Board.position.y - m_BoardMargin, 0);

        // Removed conditional branches from nested loop.
        for (int j = 1; j < BOARD_ROW; ++j) // This was if (i == 0 && j != 0)
            m_BoardArray[0, j].BoardPosition = new Vector3(m_BoardArray[0, 0].BoardPosition.x, m_BoardArray[0, j - 1].BoardPosition.y - 0.615f);

        for (int i = 1; i < BOARD_COL; i++) // This was if (i != 0 && j == 0)
            m_BoardArray[i, 0].BoardPosition = new Vector3(m_BoardArray[i - 1, 0].BoardPosition.x + 0.615f, m_BoardArray[0, 0].BoardPosition.y);

        // First row and column are already set in the previous couple of loops, so we start both indices at 1.
        for (int i = 1; i < BOARD_COL; i++)
        {
            for (int j = 1; j < BOARD_ROW; j++)
            {
                m_BoardArray[i, j].BoardPosition = new Vector3(m_BoardArray[i - 1, j].BoardPosition.x + 0.615f, m_BoardArray[i, j - 1].BoardPosition.y - 0.615f);
            }
        }

        InstantiateBlocks();
        ClearCurrentBlock(null);
    }

    void Update()
    {
        m_ScoreView.text = m_GameScore.ToString();
        CheckBoard();

        // Moved this call to be inside ClearCurrentBlock().
        //CallABlock();

        m_DropDelay -= DROP_DELAY_DEC;

        if (Input.GetKeyDown(KeyCode.N))
            NewGame();

#if ENABLE_DEBUG_BLOCK_CODE
        if (Input.GetKeyDown(KeyCode.Alpha0))
            ForceBlockValue = 0;
        else if (Input.GetKeyDown(KeyCode.Alpha1))
            ForceBlockValue = 1;
        else if (Input.GetKeyDown(KeyCode.Alpha2))
            ForceBlockValue = 2;
        else if (Input.GetKeyDown(KeyCode.Alpha3))
            ForceBlockValue = 3;
        else if (Input.GetKeyDown(KeyCode.Alpha4))
            ForceBlockValue = 4;
        else if (Input.GetKeyDown(KeyCode.Alpha5))
            ForceBlockValue = 5;
        else if (Input.GetKeyDown(KeyCode.Alpha6))
            ForceBlockValue = 6;
        else if (Input.GetKeyDown(KeyCode.Alpha7))
            ForceBlockValue = 7;
        else if (Input.GetKeyDown(KeyCode.Alpha8))
            ForceBlockValue = 8;
        else if (Input.GetKeyDown(KeyCode.Alpha9))
            ForceBlockValue = 9;
#endif
    }
}
