﻿using System.Collections;
using UnityEngine;

public class Block : MonoBehaviour
{
    [Header("Sprites")]
    [SerializeField]
    private Sprite[] m_Blocks = new Sprite[9];

    [Header("Game")]
    private Game m_GameScript;
    
    private int m_Value;
    private int m_PosX;
    private int m_PosY;
    private int m_TPosX;
    private int m_TPosY;

    private int m_MovementFixer;
    private bool m_FirstFall = true;

    private Coroutine m_DropBlockCoroutine;


    public void SetParent(Game parent)
    {
        m_GameScript = parent;
    }

    public void Setup(int forceValue = 0)
    {
        m_GameScript.BlockCount[0]++;

        if (m_GameScript.ForceBlockValue == 0)
        {
            if (m_GameScript.BlockCount[0]>20)
            for (int i = 1; i < m_GameScript.BlockCount.Length; i++)
            {
                if (m_GameScript.BlockCount[i] < m_GameScript.BlockCount[0] * 0.06f)
                {
                    m_Value = i;
                    break;
                }
            }

            if (m_Value == 0)
            {
                int j = Random.Range(1, 10);
                if (m_GameScript.BlockCount[j] > m_GameScript.BlockCount[0] * 0.2f)
                {
                    while (m_GameScript.BlockCount[j] > m_GameScript.BlockCount[0] * 0.2f)
                    {
                        if (j == 9)
                        {
                            j = 1;
                        }
                        else
                        {
                            j++;
                        }
                    }
                }
                m_Value = j;
            }

            m_FirstFall = true;
        }

        //m_Value = (forceValue == 0) ? (Random.Range(0, 90) / 10) + 1 : forceValue;
        GetComponent<SpriteRenderer>().sprite = m_Blocks[m_Value - 1];
        m_GameScript.BlockCount[m_Value]++;

        Debug.Log("Setup block " + gameObject.name + " with " + (forceValue == 0 ? "random value: " : "forced value: ") + m_Value);

        m_PosX = 2;
        m_PosY = 0;

        StartDropBlock();
    }

    public void StartDropBlock()
    {
        if (m_DropBlockCoroutine == null && !m_GameScript.BoardArray[m_PosX, m_PosY].DestroyBlock)
            {
            Debug.Log("<color=green>" + gameObject.name + ".StartDropBlock()</color>");
            m_DropBlockCoroutine = StartCoroutine(DropBlock());
        }
    }

    public void StopDropBlock()
    {
        if (m_DropBlockCoroutine != null)
        {
            Debug.Log("<color=yellow>" + gameObject.name + ".StopDropBlock()</color>");
            StopCoroutine(m_DropBlockCoroutine);
            m_DropBlockCoroutine = null;
        }
    }

    private void BlockMovement()
    {
        if (m_MovementFixer > 3)
            m_MovementFixer = 0;

        if (CanMoveDown())
        {
            if (!m_FirstFall) //fall faster when it's not the first time that the block is falling.
                m_PosY++;

            if (Input.GetAxisRaw("Vertical") < 0.0f || Input.touchCount!=0 && Input.GetTouch(0).deltaPosition.y < -40)
            {
                m_PosY++;
            }
            else
            {
                #region keypad input
                if (CanMoveLeft() && Input.GetAxisRaw("Horizontal") < 0.0f && m_FirstFall)
                {
                    m_MovementFixer++;
                    if (m_MovementFixer == 3)
                        m_PosX--;
                }

                if (CanMoveRight() && Input.GetAxisRaw("Horizontal") > 0.0f && m_FirstFall)
                {
                    m_MovementFixer++;
                    if (m_MovementFixer == 3)
                        m_PosX++;
                }
                #endregion

                #region Touch Input

                if (CanMoveLeft() && Input.touchCount != 0 && Input.GetTouch(0).deltaPosition.x < -10 && m_FirstFall)
                {
                    m_MovementFixer++;
                    if (m_MovementFixer == 3)
                        m_PosX--;

                }

                if (CanMoveRight() && Input.touchCount != 0 && Input.GetTouch(0).deltaPosition.x > 10 && m_FirstFall)
                {
                    m_MovementFixer++;
                    if (m_MovementFixer == 3)
                        m_PosX++;
                }

                #endregion
            }
        }
    }

    private bool CanMoveLeft()
    {
        return (m_PosX > 0) && (m_GameScript.BoardArray[m_PosX - 1, m_PosY].BlockValue == 0);
    }

    private bool CanMoveRight()
    {
        return (m_PosX < Game.BOARD_COL - 1) && (m_GameScript.BoardArray[m_PosX + 1, m_PosY].BlockValue == 0);
    }

    private bool CanMoveDown()
    {
        return (m_PosY < Game.BOARD_ROW - 1) && (m_GameScript.BoardArray[m_PosX, m_PosY + 1].BlockValue == 0);
    }

    private IEnumerator DropBlock()
    {
        while (true)
        {
            yield return new WaitForSeconds(m_GameScript.DropDelay);

            //Debug.Log("DropBlock called inside " + gameObject.name);
            if (CanMoveDown())
            {
                m_PosY++;
            }
            else
            {
                Debug.Log("DropBlock called inside " + gameObject.name + " <color>STOP!</color>");

                StopDropBlock();
                m_GameScript.ClearCurrentBlock(this);
            }
        }
    }

    public void DisableBlock()
    {
        m_GameScript.BoardArray[m_PosX, m_PosY].Reset();

        Reset();
        gameObject.SetActive(false);
    }

    public void Reset()
    {
        StopDropBlock();

        m_GameScript.BlockCount[0]--;
        m_GameScript.BlockCount[m_Value]--;

        transform.position = Vector3.zero;
        m_Value = 0;
        m_PosX = 0;
        m_PosY = 0;
        m_TPosX = 0;
        m_TPosY = 0;
        m_MovementFixer = 0;
    }

    private void BoardUpdate()
    {
        if (CanMoveDown())
        {
            m_GameScript.BoardArray[m_PosX, m_PosY].BlockValue = 0;
        }
        else
        {
            // This condition is always false... m_TPosX is equal to m_PosX everytime, as we m_TPosX = m_PosX; before calling this BoardUpdate() method.
            //if (m_TPosX != m_PosX && m_TPosY != m_PosY && m_GameScript.BoardArray[m_TPosX, m_TPosY].BlockValue == m_GameScript.BoardArray[m_PosX, m_PosY].BlockValue)
            //{
            //    m_GameScript.BoardArray[m_TPosX, m_TPosY].BlockValue = 0;
            //}

            // And I think you meant the following (set the previous block position back to zero and update the new block position with its value).
            m_GameScript.BoardArray[m_TPosX, m_TPosY].BlockValue = 0;
            m_TPosX = m_PosX;
            m_TPosY = m_PosY;

            m_GameScript.BoardArray[m_PosX, m_PosY].BlockValue = m_Value;

            // We don't need this.
            //m_GameScript.BoardArray[m_PosX, m_PosY].Object = gameObject;
        }
    }

    //void Start()
    //{
    //    // This is being set inside SetParent().
    //    //m_GameScript = m_Game.GetComponent<Game>();

    //    // No need to call BoardUpdate() here.
    //    //BoardUpdate();

    //    // Set block value and sprite when it's enabled.
    //    //SetValue();
    //    //GetComponent<SpriteRenderer>().sprite = m_Blocks[m_Value - 1];
    //}

    void Update()
    {
        if (m_GameScript.BoardArray[m_PosX, m_PosY].DestroyBlock)
        {
            DisableBlock();
            return;
        }
        if (!CanMoveDown())
        {
            m_FirstFall = false;
        }

        // Don't call DropBlock() here, since we started it (coroutine) inside Setup().
        //DropBlock();

        BoardUpdate();
        BlockMovement();
    }

    void FixedUpdate()
    {
        transform.position = m_GameScript.BoardArray[m_PosX, m_PosY].BoardPosition;
    }
}
