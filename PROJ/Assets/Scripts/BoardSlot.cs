﻿using UnityEngine;

public class BoardSlot
{
    private Vector3 m_BoardPosition;
    private int m_BlockValue;
    private bool m_DestroyBlock;

    #region GET SET METHODS
    public Vector3 BoardPosition
    {
        get { return m_BoardPosition; }
        set { m_BoardPosition = value; }
    }

    public int BlockValue
    {
        get { return m_BlockValue; }
        set { m_BlockValue = value; }
    }

    public bool DestroyBlock
    {
        get { return m_DestroyBlock; }
        set { m_DestroyBlock = value; }
    }
    #endregion

    public BoardSlot()
    {
        m_BoardPosition = Vector3.zero;

        Reset();
    }

    public void Reset()
    {
        // We must not reset m_BoardPosition otherwise the blocks will be all wrong.
        m_BlockValue = 0;
        m_DestroyBlock = false;
    }
}
